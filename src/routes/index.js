const Router = require('@koa/router');

const userRoutes = require('./userRoutes');
const ticketRouter = require('./ticketRoutes');
const router = new Router();

router.use(userRoutes.routes());
router.use('/game', ticketRouter.routes());

module.exports = router;
