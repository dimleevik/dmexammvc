const { createHash } = require('crypto');
const Router = require('@koa/router');

const Redis = require('../services/redisService');
const mongo = require('../models/');
const authBodyValidator = require('../middlewares/authBodyValidator');
const cookieChecker = require('../middlewares/cookiesCheck');
const router = new Router();
//Роут регистрации
router.post(
  '/signup',
  authBodyValidator, //Валидация
  async function ({ request, response }) {
    try {
      let hash = createHash('sha256');
      let { login, password } = request.body;
      //Проверка пользователя на дубликат
      let [user] = await mongo.users.find({ login });
      if (user) {
        response.status = 400;
        response.body = { Error: 'User exists' };
        return;
      }
      hash.update(password);
      //Если пользователя в БД нет - создаем
      let result = await mongo.users.create({
        login,
        password: hash.digest('hex'),
      });
      response.body = { Status: 'OK', Message: { result } };
    } catch (e) {
      response.status = 500;
      response.body = { Message: e.message };
    }
  }
);

router.post(
  '/signin',
  authBodyValidator,
  async function ({ request, response, cookies }) {
    try {
      let hash = createHash('sha256');
      let { login, password } = request.body;
      //Поиск пользователя
      let [user] = await mongo.users.find({ login });
      if (!user) {
        response.status = 400;
        response.body = { Error: 'Can`t find user' };
        return;
      }
      hash.update(password);
      if (user.password === hash.digest('hex')) {
        //Если введенный пароль и пароль из бд совпадают - создаем cookies
        hash = createHash('sha256');
        response.body = { Status: 'OK', Message: { user } };
        hash.update((Math.random() * 10000000).toString());
        let cookieHash = hash.digest('base64');
        //Записываем куки в редис и ставим TTL
        Redis.aset(cookieHash, login);
        Redis.aexpire(cookieHash, 3600);
        response.body = {
          Status: 'OK',
          Message: { user, cookie: cookieHash },
        };
        cookies.set('foo', cookieHash, { httpOnly: false });
      } else {
        response.status = 400;
        response.body = { Error: 'Wrong password' };
      }
    } catch (e) {
      response.status = 500;
      response.body = { Message: e.message };
    }
  }
);

router.get('/balance', cookieChecker, ({ request, response, cookies }) => {
  try {
    //Роут для получения баланса
    response.body = response.data.balance;
  } catch (e) {
    response.status = 500;
    response.body = e.message;
  }
});

module.exports = router;
