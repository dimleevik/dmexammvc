const Router = require('@koa/router');

const cookieChecker = require('../middlewares/cookiesCheck');
const ticketGenerator = require('../services/ticketGenerator');
const gameBodyValidator = require('../middlewares/gameValidator');
const constants = require('../../constants/constants.json');
const mongo = require('../models/');
const router = new Router();
//Роут покупки билетов
router.post(
  '/',
  cookieChecker,
  gameBodyValidator,
  async function ({ request, response }) {
    try {
      const { count } = request.body;
      if (!count) {
        response.status = 400;
        response.body = { message: 'Field `count` required' };
      }
      const { balance, _id } = response.data;
      let ticketCount = Math.floor(balance / constants.PRICE);
      if (ticketCount < count) {
        throw { message: 'You need more money' };
      }
      //Если баланса хватает, то генерируем нужное количество билетов
      let [user] = await mongo.users.find({ _id });
      user.balance = user.balance - count * constants.PRICE;
      for (let i = 0; i < count; i++) {
        //Записываем билеты и связываем их с текущим пользователем
        let ticket = await mongo.tickets.create({
          ticket: ticketGenerator(),
          userId: _id,
        });
        user.tickets.push(ticket);
        if (!user.activeTicket) {
          user.activeTicket = ticket._id;
        }
        await user.save();
      }
      response.body = { Message: 'Success' };
    } catch (e) {
      response.status = 500;
      response.body = e.message;
    }
  }
);

router.get('/state', cookieChecker, async function ({ request, response }) {
  try {
    //Получение данных о билетах и текущем билете
    const { _id } = response.data;
    let [user] = await mongo.users.find({ _id });
    let [activeTicket] = await mongo.tickets.find({ _id: user.activeTicket });
    response.body = {
      tickets: user.tickets.length,
      activeTicket,
    };
  } catch (e) {
    response.status = 500;
    response.body = e.message;
  }
});

router.post(
  '/cell',
  cookieChecker,
  gameBodyValidator,
  async function ({ request, response }) {
    try {
      //Закрашивание клеток
      const { id, cell } = request.body;
      if (!id && !cell) {
        response.status = 400;
        response.body = { message: 'Fields `id`/`cell` required' };
      }
      const { balance, _id } = response.data;
      const [ticket] = await mongo.tickets.find({ _id: id });
      if (!ticket || ticket.isUsed) {
        //Проверяем заканчивалась ли по этому билету игра
        response.status = 400;
        response.body = { message: 'Ticket not exist/valid' };
        return;
      }
      if (ticket.userId.toString() !== _id.toString()) {
        //Проверям есть ли связь с текущим пользователем
        response.status = 400;
        response.body = { message: 'User and ticket not linked' };
        return;
      }
      //Записываем билет в текущий у пользователя
      const [user] = await mongo.users.find({ _id });
      user.activeTicket = ticket._id;
      let x = Math.floor(cell / 3);
      let y = cell % 3;
      //Записываем стертую ячейку у билета
      if (ticket.userOpenedTicket.includes([x, y].join(','))) {
        response.status = 400;
        response.body = { message: 'This cell already opened' };
        return;
      }
      ticket.userOpenedTicket.push([x, y].join(','));
      await Promise.all([ticket.save(), user.save()]); //Сохраняем измененные данные
      response.body = ticket;
    } catch (e) {
      response.status = 500;
      response.body = e.message;
    }
  }
);

router.post('/end', cookieChecker, async function ({ request, response }) {
  try {
    //Завершаем игру по билету
    const { id } = request.body;
    if (!id && !cell) {
      response.status = 400;
      response.body = { message: 'Field `id` required' };
    }
    const { balance, _id } = response.data;
    const [ticket] = await mongo.tickets.find({ _id: id });
    let sum = 0;
    //Проверка связи пользователь-билет
    if (ticket.userId.toString() !== _id.toString()) {
      response.status = 400;
      response.body = { message: 'User and ticket not linked' };
      return;
    }
    //Проход по массиву отокрытых ячеек и подсчет баллов
    ticket.userOpenedTicket.forEach((opened) => {
      let [x, y] = opened.split(',');
      sum += ticket.ticket[x][y];
    });
    ticket.isUsed = true;
    await ticket.save();
    response.body = { Sum: sum };
  } catch (e) {
    response.status = 500;
    response.body = e.message;
  }
});

module.exports = router;
