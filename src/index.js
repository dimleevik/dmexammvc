process.env = require('../configs/index');
const HTTPService = require('./http/index');
const router = require('./routes/index');
const mongo = require('./models');
try {
  //Подключение к БД
  mongo.mongoose.connect(
    `mongodb://${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.DB_NAME}`,
    {
      useNewUrlParser: true,
    }
  );
  //Инициализация сервера
  const http = new HTTPService(router);
  //Запуск сервера
  http.run();
} catch (e) {
  console.log(e.message);
}
