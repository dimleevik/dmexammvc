async function gameBodyValidator({ request, response }, next) {
  try {
    let { cell, count } = request.body;
    if (cell && cell > 8) {
      throw { message: 'Field `cell` must be less than 8' };
    }
    if (count && count > 10) {
      throw { message: 'Field `status` must be less than 10' };
    }
    await next();
  } catch (e) {
    response.status = 400;
    response.body = e;
  }
}

module.exports = gameBodyValidator;
