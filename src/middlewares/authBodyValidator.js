async function authBodyValidator({ request, response }, next) {
  try {
    let { login, password } = request.body;
    if (typeof login === 'undefined' || typeof password === 'undefined') {
      throw { Error: 'Fields login/password required' };
    }
    if (login.length < 3 || login.length > 15) {
      throw { Error: 'Field login must be less than 15 and greater than 3' };
    }
    if (password.length < 6 || password.length > 20) {
      throw { Error: 'Field password must be less than 20 and greater than 6' };
    }
    await next();
  } catch (e) {
    response.status = 400;
    response.body = e;
  }
}

module.exports = authBodyValidator;
