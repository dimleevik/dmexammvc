const redis = require('../services/redisService');
const mongo = require('../models');

async function cookiesCheck({ response, cookies }, next) {
  try {
    let cookieData = cookies.get('foo');
    let login = await redis.aget(cookieData);
    let [user] = await mongo.users.find({ login });
    if (!user) {
      throw { message: 'You need to be logged in' };
    }
    response.data = user;
    await next();
  } catch (e) {
    response.status = 400;
    response.message = e.message;
  }
}

module.exports = cookiesCheck;
