const redis = require('redis');
const { promisify } = require('util');

const client = redis.createClient({
  host: process.env.REDIS_HOST,
  port: process.env.REDIS_HOST,
});

client.on('error', function (error) {
  console.error(error);
});

module.exports = {
  aget: promisify(client.get).bind(client),
  aset: promisify(client.set).bind(client),
  aexpire: promisify(client.expire).bind(client),
};
