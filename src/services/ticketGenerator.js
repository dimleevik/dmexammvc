const { ARRAY_TICKET_CELLS } = require('../../constants/constants.json');

const cols = ['i', 'j'];
const rows = ['i', 'j', 0, 1, 2, '-i', '-j'];
function ticketGenerator(id) {
  try {
    //Создание 2д массива
    let array = [new Array(3), new Array(3), new Array(3)];
    let x = cols[Math.floor(Math.random() * cols.length)]; //Случайный выбор горизонтали или вертикали
    let y = rows[Math.floor(Math.random() * rows.length)]; //Случайный выбор заполнения ряда
    let winNum =
      ARRAY_TICKET_CELLS[Math.floor(Math.random() * ARRAY_TICKET_CELLS.length)]; //Рандом числа которым будем заполнять ряд
    let nums = ARRAY_TICKET_CELLS.filter((item) => {
      //Удаление числа которым будем заполнять ряд
      if (item !== winNum) {
        return item;
      }
    });
    //Заполнение ряда:
    for (let i = 0; i < 3; i++) {
      for (let j = 0; j < 3; j++) {
        if (x === 'i' && (typeof y === 'number' || y === 'i' || y === '-i')) {
          y = typeof y === 'number' ? y : 0;
          array[y][j] = winNum;
        } else if (
          x === 'j' &&
          (typeof y === 'number' || y === 'j' || y === '-j')
        ) {
          y = typeof y === 'number' ? y : 0;
          array[i][y] = winNum;
        } else if (
          ((x === 'j' && y === 'i') || (x === 'i' && y === 'j')) &&
          i === j
        ) {
          array[i][j] = winNum;
        } else if (
          ((x === 'j' && y === '-i') || (x === 'i' && y === '-j')) &&
          i === 2 - j
        ) {
          array[i][j] = winNum;
        }
      }
    }
    //Заполнения оставшихся пустых элементов массива
    for (let i = 0; i < 3; i++) {
      for (let j = 0; j < 3; j++) {
        if (!array[i][j]) {
          array[i][j] = nums[Math.floor(Math.random() * nums.length)];
        }
      }
    }
    return array;
  } catch (e) {
    console.log(e);
  }
}

module.exports = ticketGenerator;
