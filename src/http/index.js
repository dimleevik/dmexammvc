const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const app = new Koa();

class HTTPService {
  constructor(router) {
    app.use(bodyParser());
    app.use(router.routes());
  }
  run() {
    app.listen(process.env.PORT, () => {
      console.log(`listen on ${process.env.PORT}`);
    });
  }
}

module.exports = HTTPService;
