const mongoose = require('mongoose');

const users = require('./users');
const tickets = require('./tickets');

module.exports = {
  mongoose,
  users,
  tickets,
};
