const { Schema, model } = require('mongoose');

const ticketSchema = new Schema({
  ticket: {
    type: Array,
    required: true,
  },
  userOpenedTicket: {
    type: Array,
    default: [],
  },
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'user',
  },
  isUsed: {
    type: Boolean,
    default: false,
  },
});

module.exports = model('ticket', ticketSchema);
