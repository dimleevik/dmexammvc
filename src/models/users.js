const { Schema, model } = require('mongoose');

const userSchema = new Schema({
  login: {
    type: String,
    required: true,
    index: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  balance: {
    type: Number,
    default: 10,
  },
  tickets: [{ type: Schema.Types.ObjectId, ref: 'ticket' }],
  activeTicket: {
    type: Schema.Types.ObjectId,
    default: null,
  },
});

module.exports = model('user', userSchema);
