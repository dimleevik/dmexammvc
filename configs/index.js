module.exports = {
  PORT: 3000,
  MONGO_PORT: 27017,
  MONGO_HOST: 'localhost',
  DB_NAME: 'dmexam',
  REDIS_HOST: '127.0.0.1',
  REDIS_PORT: 6379,
};
